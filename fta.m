function [A, B] = fta(pv1, pv2, Iv, t1, t2)
    M = [[1, -log(t1)]; [1, -log(t2)]];
    for ii = 1:length(pv1)   
        b = [log(pv1(ii)) + t1*Iv(ii); log(pv2(ii)) + t2*Iv(ii)];
        
        c = real(inv(M)*b);

        A(ii) = exp(c(1));
        B(ii) = c(2);
    end
end

