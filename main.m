clc
clear all
close all

%% Parameter %%
D = 1/2;
Dr = linspace(0.9,1.1,1E2)*D;
%t = linspace(0.1,7.5,100);
t = 0.1:0.1:10;
    
for ii = 1:1:80
    ii
    n = [10,15,20+ii];
    [Dg1(ii), Dg2(ii), Dl(ii), Dfg(ii), Dfl(ii)] = Destimation(t, n, Dr);
    tr(ii) = t(n(3));
end

%% Plot %%
figure
loglog(tr, abs(Dg1-D)/D, 'k', 'LineWidth', 2)
hold all
loglog(tr, abs(Dg2-D)/D, 'k--', 'LineWidth', 2)
hold all
loglog(tr, abs(Dl-D)/D, 'r', 'LineWidth', 2)
hold all
loglog(tr, abs(Dfg-D)/D, 'g--', 'LineWidth', 2)
hold all
loglog(tr, abs(Dfl-D)/D, 'g', 'LineWidth', 2)
xlabel('$t$', 'Interpreter', 'latex')
ylabel('$\frac{|D_{es} - D|}{D}$', 'Interpreter', 'latex')
legend('2nd moment', 'gauss', 'finite time gauss', 'ldf', 'Interpreter', 'latex')
set(gca, 'fontsize', 40)