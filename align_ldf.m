function [e1, e2, e3, e4] = align_ldf(d1, d2, d3, d4)
    d{1} = d1; d{2} = d2; d{3} = d3; d{4} = d4;

    %% range %%
    for ii = 1:4
        vmin(ii) = min(d{ii}.v);
        vmax(ii) = max(d{ii}.v);
    end
    v = linspace(max(vmin), min(vmax), 1000E0);

    %% cubic spline analysis %%
    for ii = 1:4
        cs{ii} = spline(d{ii}.v,[0 d{ii}.val 0]);
        f{ii} = ppval(cs{ii},v);
    end    

    %% For Transmission %%
    e1.v = v; e1.val = f{1};
    e2.v = v; e2.val = f{2};
    e3.v = v; e3.val = f{3};
    e4.v = v; e4.val = f{4};
end