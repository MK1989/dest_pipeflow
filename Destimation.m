function [Dg1, Dg2, Dl, Dfg, Dfl] = Destimation(t, n, Dr)
    %% Parameter %%
    nl = length(n);

    %% Load Experimental Data %%
    for ii = 1:3
        sim{ii} = load(['Simulation_5/sim', num2str(t(n(ii))), '.mat']);
        sim{ii}.val = sim{ii}.val/trapz(t(n(ii))*sim{ii}.v, sim{ii}.val/t(n(ii)));
    end
    
    %% Load Ratefunction %% 
    Iv = load(['Ratefunction/Iv.mat']);
    
    %% Algin %%
    [simal{1}, simal{2}, simal{3}, Ival] = align_ldf(sim{1}, sim{2}, sim{3}, Iv);

    %% D calculation by variance %%
    m1 = trapz(t(n(3))*sim{3}.v, sim{3}.v.*sim{3}.val);
    m1 = 0;
    m2 = trapz(t(n(3))*sim{3}.v, t(n(3))*sim{3}.v.^2.*sim{3}.val);
    Dg1 = ((1/4)/(48*(m2 - m1^2)/t(n(3)))*2);
    
    %% D Gauss approximation %%
    for ii = 1:length(Dr)
        K = 1/(4*48*Dr(ii));
        gauss = t(n(3))/sqrt(4*pi*K*t(n(3)))*exp(-t(n(3))^2*simal{3}.v.^2/(4*K*t(n(3))));
        lms(ii) = sum((simal{3}.val - gauss).^2);
    end   
    f = find(lms == min(lms), 1, 'first');
    Dg2 = Dr(f);
    
    %% D ldf calculation %%
    for ii = 1:length(Dr)
        lms(ii) = sum((simal{3}.val - max(simal{3}.val)*exp(-t(n(3))*Dr(ii)*Ival.val)).^2);
    end
    f = find(lms == min(lms), 1, 'first');
    Dl = Dr(f);
    
    %% D gauss finite time fit %%
    for ii = 1:length(Dr)
        K = 1/(4*48*Dr(ii));
        Igauss = simal{3}.v.^2/(4*K);
        
        [A, B] = fta(simal{1}.val, simal{2}.val, Igauss, t(n(1)), t(n(2)));
        lms(ii) = sum((simal{3}.val - A.*t(n(3)).^(-B).*exp(-t(n(3))*Igauss)).^2);
    end   
    f = find(lms == min(lms), 1, 'first');
    Dfg = Dr(f);
    
    %% D ldf finite time fit %%
    for ii = 1:length(Dr)
        [A, B] = fta(simal{1}.val, simal{2}.val, Dr(ii)*Ival.val, t(n(1)), t(n(2)));
        lms(ii) = sum((simal{3}.val - A.*t(n(3)).^(-B).*exp(-t(n(3))*Dr(ii)*Ival.val)).^2);
    end   
    f = find(lms == min(lms), 1, 'first');
    Dfl = Dr(f);

    %% Plot %%
%     K = 1/(4*48*Dg1);
%     gauss = t(n(3))/sqrt(4*pi*K*t(n(3)))*exp(-t(n(3))^2*simal{3}.v.^2/(4*K*t(n(3))));
%     K = 1/(4*48*Dg2);
%     gauss2 = t(n(3))/sqrt(4*pi*K*t(n(3)))*exp(-t(n(3))^2*simal{3}.v.^2/(4*K*t(n(3))));
%     figure
%     semilogy(simal{3}.v(1:10:end), simal{3}.val(1:10:end), 'o', 'LineWidth', 2)
%     hold all
%     semilogy(simal{3}.v, gauss, 'g--', 'LineWidth', 2)
%     hold all
%     semilogy(simal{3}.v-m1, gauss2, 'b--', 'LineWidth', 2)
%     hold all
%     semilogy(simal{3}.v, max(simal{3}.val)*exp(-t(n(3))*Dl*Ival.val), 'r--', 'LineWidth', 2)
%     xlabel('$v$', 'Interpreter', 'latex')
%     ylabel('$p(v,t)$', 'Interpreter', 'latex')
%     set(gca, 'fontsize', 40)
%     pause
%     close all
end